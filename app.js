var franc = require("franc");
var fs = require("fs");
var _ = require("underscore");
var walker = require("walker");

walker('D:\\Projects\\language-detection\\test-data\\arosa').on('file', function (file,stat) {

  fs.readFile(file,"utf8", function (err,data) {

    if (err)
      console.log(err);

  var jObj = JSON.parse(data);

  var values = [];

  extractText(jObj)
//  console.log(values);
  var langString = values.join(',');
  console.log(file + ": " + franc(langString,{whitelist:["eng","deu"]}));

  function extractText (jObj)
  {
    var textProperties = ['description', 'name', 'unitText', 'text'];
    for (var key in jObj)
    {
      if (_.contains(textProperties,key))
        values.push(jObj[key]);
      else if (_.isArray(jObj[key]))
      {
        for (var obj of jObj[key])
          extractText(obj);
      }
      else if (_.isObject(jObj[key]))
        extractText(jObj[key]);

    }

  }

  });


});
